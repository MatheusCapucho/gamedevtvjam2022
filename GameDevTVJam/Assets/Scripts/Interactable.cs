using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Interactable : MonoBehaviour
{
    [HideInInspector] public bool isInRange;

    private IPossessed interactable;

    private void Start()
    {
        interactable = GetComponent<IPossessed>();
    }

    void Update()
    {
        if (isInRange)
            if (!GameManager.Instance.AlreadyInPossession && Input.GetKeyDown(KeyCode.Space))
            {              
                interactable.TakeControl();

            } else if (GameManager.Instance.AlreadyInPossession && Input.GetKeyDown(KeyCode.Space))
            {
                interactable.LoseControl();
            }
                
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            
            isInRange = true;
        }

    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            
            isInRange = false;
        }
        
    }

}
