using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartNewLevel : MonoBehaviour
{

    [SerializeField]
    private string sceneName = "Digite Aqui!!";
    [SerializeField]
    private bool unlockgate = false;
    [SerializeField]
    private int gateToUnlock = -1;

    private void LoadWithName(string name)
    {
        SceneManager.LoadScene(name);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (unlockgate)
            HeartMenu.Instance.SetNextLevel(gateToUnlock);
                  

        if (other.CompareTag("Player"))
            LoadWithName(sceneName);
    }

}
