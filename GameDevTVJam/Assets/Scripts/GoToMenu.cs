using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoToMenu : MonoBehaviour
{
    void Start()
    {
        StartCoroutine(EndThis());
    }

    IEnumerator EndThis()
    {
        yield return new WaitForSeconds(15f);

        SceneManager.LoadScene("MainMenu");

        yield return null;

    }
}
