using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    public GameObject portal;
    public GameObject Player;
    private Transform movepoint;

    private void Start()
    {
        movepoint = GameObject.Find("MovePoint").transform;
        Player = GameObject.Find("Player");
    }

    private void OnTriggerEnter2D (Collider2D Collision)
    {
        if (Collision.gameObject.CompareTag("Player") || Collision.gameObject.CompareTag("Ghostbuster"))
            StartCoroutine(enumerator());
    }
      IEnumerator enumerator()
    {
        portal.GetComponent<Collider2D>().enabled = false;
        Player.transform.position = portal.transform.position;
        movepoint.position = Player.transform.position;
        Player.transform.rotation = portal.transform.rotation;
        yield return new WaitForSeconds(3f);
        portal.GetComponent<Collider2D>().enabled = true;
        yield return null;
    }
}