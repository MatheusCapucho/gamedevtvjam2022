using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TypeWriterEffect : MonoBehaviour
{

    [SerializeField]
    private float typeSpeed = 20f;

    public Coroutine RunTypeWriterEffect(string txt, TMP_Text tmp)
    {
        return StartCoroutine(TypeText(txt, tmp));
    }

    private IEnumerator TypeText(string txt, TMP_Text tmp)
    {
        float t = 0f;
        int index = 0;
        tmp.text = string.Empty;

        yield return new WaitForSeconds(.5f);

        while (index < txt.Length)
        {
            t += Time.deltaTime * typeSpeed;
            index = Mathf.Clamp(Mathf.FloorToInt(t), 0, txt.Length);
            tmp.text = txt.Substring(0, index);
            yield return null;
        }

        tmp.text = txt;

    }

}
