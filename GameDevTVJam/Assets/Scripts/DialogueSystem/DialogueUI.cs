using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DialogueUI : MonoBehaviour
{

    [SerializeField]
    private TMP_Text tmp;
    private TypeWriterEffect typeWriterEffect;

    [SerializeField]
    private GameObject dialogueBox;

    
    private void Awake()
    {
        CloseDialogueBox();
        typeWriterEffect = GetComponent<TypeWriterEffect>();
    }

    public void ShowDialogue(DialogueObject dialogueObj)
    {
        StartCoroutine(ShowAllDialogues(dialogueObj));
    }

    IEnumerator ShowAllDialogues(DialogueObject dialogueObj)
    {

        yield return new WaitForSeconds(1f);

        foreach (var phrase in dialogueObj.Dialogue)
        {

            yield return typeWriterEffect.RunTypeWriterEffect(phrase, tmp);
           
            yield return new WaitUntil(() => Input.anyKeyDown);
        }

        CloseDialogueBox();

    }
    private void CloseDialogueBox()
    {
        tmp.text = string.Empty;
        dialogueBox.SetActive(false);
        GameManager.Instance.PlayerCanMove = true;
    }

    public void OpenDialogueBox(DialogueObject dialogueObj)
    {
        dialogueBox.SetActive(true);
        ShowDialogue(dialogueObj);
        GameManager.Instance.PlayerCanMove = false;
    }

}
