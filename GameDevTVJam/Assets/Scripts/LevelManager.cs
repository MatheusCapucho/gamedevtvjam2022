using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{

    [SerializeField]
    private int totalPressurePlates;
    private int activePressurePlates;

    //[SerializeField] private GameObject lockedGate;
    [SerializeField] private GameObject openedGate;


    void Start()
    {
        activePressurePlates = 0;
    }
    
    public void CheckLevelState(int sum)
    {
        activePressurePlates += sum;
        if (activePressurePlates >= totalPressurePlates)
        {
            //Destroy(lockedGate);
            print("can end");
            openedGate.SetActive(true);
        }
           
    }

}
