using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
    [SerializeField]
    private DialogueObject dialogueObject;
    public void TriggerDialogue()
    {
        FindObjectOfType<DialogueUI>().OpenDialogueBox(dialogueObject);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
            TriggerDialogue();
    }


}
