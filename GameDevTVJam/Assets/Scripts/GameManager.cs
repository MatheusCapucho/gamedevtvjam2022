using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public static GameManager Instance { get; private set; }
    public KeyCode ghostbusterActionKey = KeyCode.Z;
    public KeyCode ghostbusterActionKey2 = KeyCode.X;

    [HideInInspector]
    public bool PlayerCanMove = true;

    [HideInInspector]
    public bool AlreadyInPossession = false;

    private void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(gameObject);
        else
            Instance = this;

        DontDestroyOnLoad(gameObject);

    }


}
