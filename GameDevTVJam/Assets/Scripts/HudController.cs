using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HudController : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI EssenceText;

    public void TextUpdate(int value)

    {
        EssenceText.text = value.ToString();
    }

}
