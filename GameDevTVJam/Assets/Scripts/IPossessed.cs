using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPossessed
{
    void TakeControl();
    void LoseControl();

   
}
