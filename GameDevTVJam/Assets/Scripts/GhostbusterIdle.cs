using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.SceneManagement;

public class GhostbusterIdle : MonoBehaviour
{

    [SerializeField][Range(1, 5)]
    private int rayDistance = 2;

    [HideInInspector]
    public int RayDistance { get => rayDistance;}

    [SerializeField]
    private LayerMask playerMask;

    private RaycastHit2D hit;

    [SerializeField]
    private Vector3 facingDirection;

    [HideInInspector] public Vector3 StartingFacingDir => facingDirection;

    private Vector3 rayStartPos;

    private LineRenderer lineRenderer;

    Animator anim;
    string currentState;
    const string IDLE = "Ghostbuster_Down";
    const string UP = "Ghostbuster_Up";
    const string LEFT = "Ghostbuster_Left";
    const string RIGHT = "Ghostbuster_Right";


    private void Awake()
    {
        lineRenderer = GetComponentInChildren<LineRenderer>();
        anim = GetComponent<Animator>();
    }

    void Start()
    {
        
        facingDirection = facingDirection.normalized;
        rayStartPos = transform.position;
        lineRenderer.SetPosition(1, facingDirection * rayDistance);

        if (facingDirection.x > 0)
            HandleAnimation(RIGHT);
        if (facingDirection.x < 0)
            HandleAnimation(LEFT);
        if (facingDirection.y > 0)
            HandleAnimation(UP);
        if (facingDirection.y < 0)
            HandleAnimation(IDLE);
        SetFacingDirection(facingDirection);

    }

    private void OnEnable()
    {
        Vector3 lastDir = GetComponent<PossessedGhostbuster>().LastDirection;
        if (lastDir == Vector3.zero)
            lastDir = facingDirection;
        SetFacingDirection(lastDir);
    }

    public void SetFacingDirection(Vector3 dir)
    {
        facingDirection = dir;
        rayStartPos = transform.position;

        if (dir.x > 0)
            HandleAnimation(RIGHT);
        if (dir.x < 0)
            HandleAnimation(LEFT);
        if (dir.y > 0)
            HandleAnimation(UP);
        if (dir.y < 0)
            HandleAnimation(IDLE);

        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, rayDistance);
        if (hit.transform != null)
        {
            lineRenderer.SetPosition(0, transform.position);
            lineRenderer.SetPosition(1, hit.point);
        }
        else
        {
            lineRenderer.SetPosition(0, transform.position);
            lineRenderer.SetPosition(1, transform.position + dir * rayDistance);
        }
    }

    void FixedUpdate()
    {
        hit = Physics2D.Raycast(rayStartPos, facingDirection, rayDistance);

        if (hit.transform != null && hit.transform.CompareTag("Player"))
            HitPlayer();

    }

    private void HitPlayer()
    {
        print("Hitei Player");

        FindObjectOfType<RestartLevel>().Restart(true);

    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        rayStartPos = transform.position + facingDirection;
        Gizmos.DrawLine(rayStartPos, rayStartPos + facingDirection * rayDistance);
    }

    private void HandleAnimation(string nextState)
    {
        if (currentState == nextState)
            return;

        anim.Play(nextState);

        currentState = nextState;

    }

}
