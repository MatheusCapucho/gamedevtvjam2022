using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GridMovement : MonoBehaviour
{

    private Vector2 input;

    [HideInInspector] public Vector2 PlayerInput => input;
   

    private Transform movePoint;
    [SerializeField] private Tilemap collisionTile;
    [SerializeField] private Tilemap groundTile;
    [SerializeField] private LayerMask stopMovementMask;
    [SerializeField] private float speed = 3f;

    // Anim Stuff

    Animator anim;
    string currentState;
    const string DOWN = "Player_Down";
    const string UP = "Player_Up";
    const string LEFT = "Player_Left";
    const string RIGHT = "Player_Right";


    private void Awake()
    {
        movePoint = GameObject.Find("MovePoint").transform;
        anim = GetComponent<Animator>();
    }

    private void Start()
    {
        movePoint.parent = null;
    }

    void Update()
    {
        if (!GameManager.Instance.PlayerCanMove) 
            return;

        input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        transform.position = Vector3.MoveTowards(transform.position, movePoint.position, speed * Time.deltaTime);
        Move(input);       
    }

    public void HandleAnim(string nextState)
    {
        if (currentState == nextState)
            return;

        anim.Play(nextState);

        currentState = nextState;
    }



    private void Move(Vector2 inpt)
    {
        if (!CanMove(inpt))
            return;

        if (inpt.magnitude > 1f)
            return;

        if (Vector3.Distance(transform.position, movePoint.position) > 0.2f)
            return;

        if (inpt.x > 0)
            HandleAnim(RIGHT);
        if (inpt.x < 0)
            HandleAnim(LEFT);
        if (inpt.y > 0)
            HandleAnim(DOWN);
        if (inpt.y < 0)
            HandleAnim(DOWN);



        movePoint.position += (Vector3) inpt;
                   
    }

    private bool CanMove(Vector2 inpt)
    {
        Vector3Int movePos = groundTile.WorldToCell(transform.position + (Vector3)inpt);
        
        Vector2 moveposV2 = new Vector2(movePos.x + 1, movePos.y + 1); // ?
        bool hasStopMovement = Physics2D.OverlapCircle(moveposV2, .6f, stopMovementMask);
       

       
        if (collisionTile.HasTile(movePos) || hasStopMovement)
            return false;

        return true;
    }

}
