using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Spike : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("HeavyBlock"))
        {
            
            other.GetComponent<Collider2D>().enabled = false;
            other.transform.position = gameObject.transform.position;

            // anim?
            
            Destroy(this.gameObject);
        }

        if (other.CompareTag("Player"))
        {
            
            FindObjectOfType<RestartLevel>().Restart(true);

        }

    }

}
