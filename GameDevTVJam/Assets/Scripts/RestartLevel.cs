using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartLevel : MonoBehaviour
{
    public void Restart(bool death)
    {
        GameManager.Instance.AlreadyInPossession = false;

        if (death)
            StartCoroutine(RestartCR());
        else
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    }

    IEnumerator RestartCR()
    {
        GameManager.Instance.PlayerCanMove = false;
        // start anim
        yield return new WaitForSeconds(2f);
        GameManager.Instance.PlayerCanMove = true;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        yield return null;
    }

    public void GoToMainMenu()
    {
        GameManager.Instance.AlreadyInPossession = false;
        GameManager.Instance.PlayerCanMove = true;
        SceneManager.LoadScene("MainMenu");
    }

}
