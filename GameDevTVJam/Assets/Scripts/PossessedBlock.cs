using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PossessedBlock : MonoBehaviour, IPossessed
{

    private GameObject player;
    private GameObject playerHolder;
    private bool isPossessed = false;
    private Interactable interactable;

    [SerializeField] 
    private Sprite possessed;
    private Sprite normalSprite;

    private void Awake()
    {
        playerHolder = GameObject.Find("PlayerHolder");
        player = GameObject.Find("Player");
        interactable = GetComponent<Interactable>();
        normalSprite = GetComponent<SpriteRenderer>().sprite;
    }

    private void Update()
    {
        if (isPossessed)
            transform.localPosition = player.transform.localPosition;
    }

    public void TakeControl()
    {
        isPossessed = true;
        GameManager.Instance.AlreadyInPossession = isPossessed;
        transform.parent = playerHolder.transform;    
        player.GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<SpriteRenderer>().sprite = possessed;
   
    }

    public void LoseControl()
    {
        isPossessed = false;
        player.GetComponent<SpriteRenderer>().enabled = true;
        transform.parent = null;
        GameManager.Instance.AlreadyInPossession = isPossessed;
        GetComponent<SpriteRenderer>().sprite = normalSprite;
    } 

}
