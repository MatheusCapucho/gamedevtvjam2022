using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HeartMenu : MonoBehaviour
{
    [SerializeField]
    private GameObject[] gates;
    private static int currentGate = 0;
    private static bool[] gatesOpen = new bool[8];

    public static HeartMenu Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;

        }
        else
        {
            Destroy(gameObject, 2f);
            return;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    private void Start()
    {
        for (int i = 0; i < gates.Length; i++)
        {
            gates[i].SetActive(!gatesOpen[i]);
        }
    }

    public void SetNextLevel(int index)
    {
        currentGate = index;
        gatesOpen[currentGate] = true;

    }




}
