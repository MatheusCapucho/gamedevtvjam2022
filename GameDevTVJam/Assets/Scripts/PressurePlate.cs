using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressurePlate : MonoBehaviour
{
    private enum ReactsTo
    {
        LIGHT_BLOCK,
        HEAVY_BLOCK,
        GHOSTBUSTER
    };

    [SerializeField]
    private ReactsTo reactsTo;

    //[SerializeField]
    private LevelManager whatThisButtonControls;

    private string expectTag;

    void Start()
    {
        switch (reactsTo)
        {
            case ReactsTo.LIGHT_BLOCK:
                expectTag = "LightBlock";
                break;
            case ReactsTo.HEAVY_BLOCK:
                expectTag = "HeavyBlock";
                break;
            case ReactsTo.GHOSTBUSTER:
                expectTag = "Ghostbuster";
                break;

        }

        whatThisButtonControls = GameObject.FindObjectOfType<LevelManager>();


    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag(expectTag))
        {
            whatThisButtonControls.CheckLevelState(1);
            print("entrou no botao certo");
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag(expectTag))
        {
            whatThisButtonControls.CheckLevelState(-1);
            print("saiu do botao certo");
        }
    }

}
