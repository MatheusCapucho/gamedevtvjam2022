using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectCollision : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("LightBlock"))
        {
            other.transform.position = other.transform.position - other.bounds.center.normalized * 0.1f;
           
        }

    }
    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("LightBlock"))
        {
            other.transform.position = other.transform.position - other.bounds.center.normalized * 0.1f;
        }

    }

}
