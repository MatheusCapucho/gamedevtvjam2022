using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderVolume : MonoBehaviour
{

    private Slider slider;

    void Start()
    {
        slider = GameObject.Find("Slider").GetComponent<Slider>();
        slider.onValueChanged.AddListener(val => AudioManager.instance.ChangeMasterVolume(val));
        
    }
}
