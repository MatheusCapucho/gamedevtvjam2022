using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemScript : MonoBehaviour
{
    private static int Essence = 0;

    private HudController HudControllerEssence;

    private void Awake()
    {
        HudControllerEssence = FindObjectOfType<HudController>();
    }

    private void Start()
    {
        Essence = 0;
    }

    private void OnTriggerEnter2D (Collider2D collision)

    {
        if (collision.gameObject.CompareTag("Player"))

        {
            Essence = Essence + 1;
            Destroy(this.gameObject);
            HudControllerEssence.TextUpdate(Essence);
        }
    }
}
