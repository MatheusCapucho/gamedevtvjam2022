using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class PossessedGhostbuster : MonoBehaviour, IPossessed
{
    private bool isPossessed = false;
    private KeyCode actionKey;
    private KeyCode actionKey2;
    private GhostbusterIdle ghostbusterIdle;
    private GameObject playerHolder;
    private GameObject player;

    [SerializeField]
    private Transform movePoint;

    [SerializeField]
    private LayerMask movableMask;

    public Vector3 LastDirection { get; private set; }

    private LineRenderer lineRenderer;

    private Vector2 playerInput;

    // Animation Stuff
    Animator anim;
    string currentState;
    const string IDLE = "Ghostbuster_Down";
    const string UP = "Ghostbuster_Up";
    const string LEFT = "Ghostbuster_Left";
    const string RIGHT = "Ghostbuster_Right";
    const string A_UP = "Ghostbuster_AUp";
    const string A_DOWN = "Ghostbuster_ADown";
    const string A_LEFT = "Ghostbuster_ALeft";
    const string A_RIGHT = "Ghostbuster_ARight";


    private void Awake()
    {
        playerHolder = GameObject.Find("PlayerHolder");
        player = GameObject.Find("Player");
        Physics2D.queriesStartInColliders = false;
        lineRenderer = GetComponentInChildren<LineRenderer>();
        anim = GetComponent<Animator>();
    }
    void Start()
    {
        actionKey = GameManager.Instance.ghostbusterActionKey;
        actionKey2 = GameManager.Instance.ghostbusterActionKey2;
        ghostbusterIdle = GetComponent<GhostbusterIdle>();
        movePoint.parent = null;
        LastDirection = ghostbusterIdle.StartingFacingDir;
    }
    void Update()
    {
        if (!isPossessed)
            return;

        transform.localPosition = player.transform.localPosition;
        movePoint.position = transform.position;
        playerInput = player.GetComponent<GridMovement>().PlayerInput;

        if (playerInput != Vector2.zero)
            LastDirection = playerInput;

        if (Input.GetKey(actionKey))
        {
            TryToMoveBlock();
            return;
        }

        if (Input.GetKey(actionKey2))
        {
            TryToMoveBlock2();
            return;
        }

        if (LastDirection.x > 0)
            HandleAnimation(A_RIGHT);
        if (LastDirection.x < 0)
            HandleAnimation(A_LEFT);
        if (LastDirection.y > 0)
            HandleAnimation(A_UP);
        if (LastDirection.y < 0)
            HandleAnimation(A_DOWN);


        GameManager.Instance.PlayerCanMove = true;

        AdjustLineRenderer(LastDirection);

    }

    private void AdjustLineRenderer(Vector3 dir)
    {
        lineRenderer.SetPosition(0, transform.position);
        //lineRenderer.SetPosition(1, transform.position + LastDirection * 5);


        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, ghostbusterIdle.RayDistance);
        if (hit.transform != null)
        {
            lineRenderer.SetPosition(1, hit.point);
        } else
        {
            lineRenderer.SetPosition(1, transform.position + dir * (ghostbusterIdle.RayDistance));
        }

    }

    private void TryToMoveBlock()
    {
        GameManager.Instance.PlayerCanMove = false;

        Vector3 dir = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        if (dir.magnitude > 1f)
            return;
        if (Vector3.Distance(transform.position, movePoint.position) > 1f)
            return;

        if (dir != Vector3.zero)
            LastDirection = dir;

        movePoint.localPosition += dir;

        if (dir.x > 0)
            HandleAnimation(RIGHT);
        if (dir.x < 0)
            HandleAnimation(LEFT);
        if (dir.y > 0)
            HandleAnimation(UP);
        if (dir.y < 0)
            HandleAnimation(IDLE);

        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, (ghostbusterIdle.RayDistance + 1), movableMask);
        
        Debug.DrawLine(transform.position, transform.position + dir * (ghostbusterIdle.RayDistance + 1), Color.green);
        AdjustLineRenderer(LastDirection);

        if (hit.transform != null)
            MoveBlock(hit, true);

    }
    private void TryToMoveBlock2()
    {
        GameManager.Instance.PlayerCanMove = false;

        Vector3 dir = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        if (dir.magnitude > 1f)
            return;
        if (Vector3.Distance(transform.position, movePoint.position) > 1f)
            return;

        if (dir != Vector3.zero)
            LastDirection = dir;

        movePoint.localPosition += dir;

        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, ghostbusterIdle.RayDistance + 1, movableMask);

        Debug.DrawLine(transform.position, transform.position + dir * 5, Color.green);
        AdjustLineRenderer(LastDirection);

        if (hit.transform != null)
            MoveBlock(hit, false);

    }

    private void MoveBlock(RaycastHit2D hit, bool pull)
    {
        //var distance = GetComponent<GhostbusterIdle>().RayDistance;

        if (pull)
            hit.transform.position = Vector3.MoveTowards(hit.transform.position, movePoint.position, (ghostbusterIdle.RayDistance + 1) * Time.deltaTime);
        else
            hit.transform.position = Vector3.MoveTowards(hit.transform.position, transform.position, -(ghostbusterIdle.RayDistance + 1) * Time.deltaTime);
    }

    private void HandleAnimation(string nextState)
    {
        if (currentState == nextState)
            return;

        anim.Play(nextState);

        currentState = nextState;

    }


    public void TakeControl()
    {
        ghostbusterIdle.enabled = false;
        player.GetComponent<SpriteRenderer>().enabled = false;
        isPossessed = true;
        transform.parent = playerHolder.transform;
        GameManager.Instance.AlreadyInPossession = isPossessed;
    }
    public void LoseControl()
    {
        isPossessed = false;
        player.GetComponent<SpriteRenderer>().enabled = true;
        transform.parent = null;
        GameManager.Instance.AlreadyInPossession = isPossessed;
        ghostbusterIdle.enabled = true;
    }
}
